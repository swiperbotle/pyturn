#!/bin/bash

echo "Starting TURN/STUN server"

turnserver -a -v -L ${TURN_IP} --server-name "${TURN_SERVER_NAME}" --realm=${TURN_REALM} -p ${TURN_PORT} --min-port ${TURN_PORT_START} --max-port ${TURN_PORT_END} ${TURN_EXTRA} -u ${TEST_TURN_USERNAME}:${TEST_TURN_PASSWORD}